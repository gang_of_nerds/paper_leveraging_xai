"""
# Copyright 2018 Professorship Media Informatics, University of Applied Sciences Mittweida
# licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# @author Richard Vogel, 
# @email: richard.vogel@hs-mittweida.de
# @created: 25.03.2020
"""
from lib import do_dataset_run_gon, RunEval
import pandas as pd
from os.path import join as pjoin


def do_uci_cancer_experiment():
    print("Cancer data set run")
    df_cancer = pd.read_csv(pjoin('datasets', "uci_cancer.csv"))
    run_uci_cancer = do_dataset_run_gon(ds=df_cancer.values,
                                        max_depth_experts=3,
                                        max_depth_ass_tree=6,
                                        min_samples_leaf_ass_tree=10,
                                        lasso_step=0.05,
                                        n_experts=4,
                                        n_runs=20,
                                        val_perc=0.,
                                        experiment_id="uci_cancer")

    df_run_uci_cancer = pd.DataFrame(run_uci_cancer, columns=RunEval._fields)
    df_run_uci_cancer.to_csv(pjoin('results', 'gon_uci_cancer_05_percent_step.csv'), index=False)


def do_uci_cancer_experiment_with_validation_set():
    print("Cancer data set run with 30 percent validation set")
    df_cancer = pd.read_csv(pjoin('datasets', "uci_cancer.csv"))
    run_uci_cancer = do_dataset_run_gon(ds=df_cancer.values,
                                        max_depth_experts=3,
                                        max_depth_ass_tree=6,
                                        min_samples_leaf_ass_tree=10,
                                        lasso_step=0.05,
                                        n_experts=4,
                                        n_runs=20,
                                        val_perc=0.3,
                                        experiment_id="uci_cancer")

    df_run_uci_cancer = pd.DataFrame(run_uci_cancer, columns=RunEval._fields)
    df_run_uci_cancer.to_csv(pjoin('results', 'gon_with_val_30_uci_cancer_05_percent_step.csv'), index=False)


def do_uci_digest_experiment():
    print("Digest data set run")
    df_digist = pd.read_csv(pjoin('datasets', "uci_digist.csv"))
    res = do_dataset_run_gon(ds=df_digist.values,
                                        max_depth_experts=3,
                                        max_depth_ass_tree=6,
                                        min_samples_leaf_ass_tree=10,
                                        n_experts=10,
                                        n_runs=20,
                                        val_perc=0.,
                                        lasso_step=0.05,
                                        experiment_id="uci_digist")

    df_run_digist = pd.DataFrame(res, columns=RunEval._fields)
    df_run_digist.to_csv(pjoin('results','gon_uci_digist_05_percent_step.csv'), index=False)


def do_fico_experiment():
    print("Fico Experiment")

    df_fico = pd.read_csv(pjoin('datasets', 'fico_dataset.csv'))

    res = do_dataset_run_gon(ds=df_fico.values,
                             max_depth_experts=3,
                             max_depth_ass_tree=6,
                             min_samples_leaf_ass_tree=10,
                             n_experts=10,
                             n_runs=20,
                             lasso_step=0.05,
                             val_perc=0.,
                             experiment_id="fico")

    df_run_digist = pd.DataFrame(res, columns=RunEval._fields)
    df_run_digist.to_csv(pjoin('results', 'gon_fico_05_percent_step.csv'), index=False)


def do_fico_experiment_with_validation_set():
    print("Fico Experiment with 30% val")

    df_fico = pd.read_csv(pjoin('datasets', 'fico_dataset.csv'))

    res = do_dataset_run_gon(ds=df_fico.values,
                             max_depth_experts=3,
                             max_depth_ass_tree=6,
                             min_samples_leaf_ass_tree=10,
                             n_experts=10,
                             n_runs=20,
                             lasso_step=0.05,
                             val_perc=0.3,
                             experiment_id="fico")

    df_run_digist = pd.DataFrame(res, columns=RunEval._fields)
    df_run_digist.to_csv(pjoin('results', 'gon_with_val_30_fico_05_percent_step.csv'), index=False)



if __name__ == '__main__':
    # do_uci_digest_experiment()
    # do_uci_cancer_experiment()
    # do_fico_experiment()
    # do_uci_cancer_experiment_with_validation_set()
    do_fico_experiment_with_validation_set()