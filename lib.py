"""
# Copyright 2018 Professorship Media Informatics, University of Applied Sciences Mittweida
# licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# @author Richard Vogel, 
# @email: richard.vogel@hs-mittweida.de
# @created: 25.03.2020
"""
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from deslib.dcs import BaseDCS
import logging
from typing import Optional, Dict, List, Union
from sklearn.metrics import accuracy_score
from os.path import join as pjoin
import datetime
import time
from collections import namedtuple
from gon import GON
from typing import List, Optional, Union
from booster.models.datatypes.file_path import FilePath
import pandas as pd
import logging
import wittgenstein
import re
from aix360.algorithms.rbm import BRCGExplainer, BooleanRuleCG



PATH_PAPER = pjoin('.', '..', '')
PATH_PAPER_FIGS = pjoin(PATH_PAPER, 'figures', )

NodeRecord = namedtuple('NodeRecord', ['id', 'depth', 'is_leaf', 'n_samples'])

RunEval = namedtuple('RunEval', ['ds_name',
                                 'clf_name',
                                 'feature_amount',
                                 'run_num',
                                 'run_time',
                                 'acc_train',
                                 'acc_test',
                                 'acc_train_assignment',
                                 'acc_test_assignment',
                                 'params_expert',
                                 'params_model',
                                 'params_ass_model',
                                 'avg_path_len', 'ovr_assignment', 'date'])


def create_assignment_problem(dcs_classifier: BaseDCS, X:np.ndarray, X_for_assignment: np.ndarray, as_ovr: bool = True):
    """
    Will generate a classification problem which will describe the assignment from data to model
    (Selection Step of Dynamic Selection)
    This can either be multiple binary problems (OVR)(I am selected / I am not selected for each ensemble member)
    or a single multiclass problem (amount of classes equals)
    """
    assert isinstance(dcs_classifier, BaseDCS), "The classifier has to be a base DCS classifier (Code: 4857348957)"

    dists, indices = dcs_classifier._get_region_competence(X_for_assignment)

    if dcs_classifier.needs_proba:
        base_predictions = dcs_classifier._predict_proba_base(X)
    else:
        base_predictions = dcs_classifier._predict_base(X)

    competences = dcs_classifier.estimate_competence(X_for_assignment, distances=dists, neighbors=indices,
                                                     predictions=base_predictions)

    selection = dcs_classifier.select(competences=competences)

    if as_ovr is True:
        """
        Create a One-VS-Rest Problem ("I am expert" / "I am not an expert")
        """
        ret = []
        classes = [cls for cls in set(selection)]
        for i, cls_name in enumerate(classes):
            expert = np.where(selection == cls_name, '1', '0').astype(np.str)
            if np.all(expert == '0'):
                logging.getLogger().warn(f"Model with idx {i} was never chosen (Code: 948340953)")
            ret.append(expert)
        return np.array(ret)

    else:  # just return classes
        return selection.astype(np.str)


def create_assignment_problem_gon(dcs_classifier: GON, X: np.ndarray, as_ovr: bool = True):
    """
    Will generate a classification problem which will describe the assignment from data to model
    (Selection Step of Dynamic Selection)
    This can either be multiple binary problems (OVR)(I am selected / I am not selected for each ensemble member)
    or a single multiclass problem (amount of classes equals)
    """
    assert isinstance(dcs_classifier, GON), "The classifier has to be a base GON classifier (Code: 4857348957)"

    assignments = dcs_classifier.assign_data_points_to_model(X=X, is_processed=False)
    selection = np.zeros(len(X), dtype=np.int)

    for model_idx, assignment in assignments.items():
        selection[assignment] = model_idx

    if as_ovr is True:
        """
        Create a One-VS-Rest Problem ("I am expert" / "I am not an expert")
        """
        ret = []
        classes = [cls for cls in set(selection)]
        for i, cls_name in enumerate(classes):
            expert = np.where(selection == cls_name, '1', '0').astype(np.str)
            if np.all(expert == '0'):
                logging.getLogger().warn(f"Model with idx {i} was never chosen (Code: 948340953)")
            ret.append(expert)
        return np.array(ret)

    else:  # just return classes
        return selection.astype(np.str)


def generate_assignment_models(assignment_problem: np.ndarray,
                               X_for_expert: np.ndarray,
                               explanation_model,
                               explanation_model_params: Dict) -> List:
    is_ovr = False
    if len(assignment_problem.shape) == 2:
        is_ovr = True

    if is_ovr:
        assert assignment_problem.shape[1] == len(
            X_for_expert), f"Sample size of X data and assignments does not match (Code: 384723847238)"
    else:
        assert assignment_problem.shape[0] == len(
            X_for_expert), f"Sample size of X data and assignments does not match (Code: 345634534034534)"

    models = []

    if is_ovr:
        for i in range(assignment_problem.shape[0]):
            model = explanation_model(**explanation_model_params)
            model.fit(X_for_expert, assignment_problem[i])
            models.append(model)
    else:
        model = explanation_model(**explanation_model_params)
        model.fit(X_for_expert, assignment_problem)
        models.append(model)

    return models


def calculate_assignments(assignment_models: List, X_for_expert: np.ndarray):
    """
    Will determine, which model will be chosen given the assignment model(s)
    """
    is_ovr = False
    if len(assignment_models) > 1:
        is_ovr = True

    if is_ovr:
        # we return the most prominent expert (which has highest probability for positive class)
        # We could in fact return a list of assignment propabilities also
        assignments = np.zeros(shape=(X_for_expert.shape[0], len(assignment_models)))
        for i, model in enumerate(assignment_models):
            preds = np.array(model.predict_proba(X_for_expert))
            certainty_for_positive = preds[:, list(model.classes_).index('1')]
            assignments[:, i] = certainty_for_positive

        # assignments = np.argwhere(assignments == 1)
        assignments = np.argmax(assignments, axis=1)
    else:
        # assignments = np.zeros(shape=(X_for_expert.shape[0]))
        preds = np.array(assignment_models[0].predict_proba(X_for_expert))
        # assignments = np.argmax(preds, axis=1)
        assignments = np.apply_along_axis(lambda x: assignment_models[0].classes_[x],
                                          0,
                                          np.atleast_1d(np.argmax(assignment_models[0].predict_proba(X_for_expert),
                                                                  axis=1))
                                          )

    return assignments.astype(np.str)


def calculate_assignment_accuracy(dcs_classifier,
                                  assignment_models: List,
                                  X_for_expert: np.ndarray,
                                  X_for_assignment: np.ndarray):
    preds = calculate_assignments(assignment_models=assignment_models,
                                  X_for_expert=X_for_expert)
    trues = create_assignment_problem(dcs_classifier=dcs_classifier,
                                      X_for_assignment=X_for_assignment, as_ovr=False)

    return accuracy_score(y_pred=preds, y_true=trues)


def calculate_assignment_accuracy_gon(dcs_classifier,
                                      assignment_models: List,
                                      X: np.ndarray,
                                      X_for_expert: np.ndarray):
    preds = calculate_assignments(assignment_models=assignment_models, X_for_expert=X_for_expert)

    trues = create_assignment_problem_gon(dcs_classifier=dcs_classifier,
                                          X=X, as_ovr=False)

    return accuracy_score(y_pred=preds, y_true=trues)


def return_node_information(tree) -> List[NodeRecord]:
    n_nodes = tree.tree_.node_count
    children_left = tree.tree_.children_left
    children_right = tree.tree_.children_right
    feature = tree.tree_.feature
    threshold = tree.tree_.threshold
    # The tree structure can be traversed to compute various properties such
    # as the depth of each node and whether or not it is a leaf.
    node_depth = np.zeros(shape=n_nodes, dtype=np.int64)
    is_leaves = np.zeros(shape=n_nodes, dtype=bool)
    n_samples = np.zeros(shape=n_nodes, dtype=np.int64)
    node_ids = np.zeros(shape=n_nodes, dtype=np.int64)

    stack = [(0, -1)]  # seed is the root node id and its parent depth
    while len(stack) > 0:
        node_id, parent_depth = stack.pop()
        node_depth[node_id] = parent_depth + 1
        n_samples[node_id] = tree.tree_.n_node_samples[node_id]
        node_ids[node_id] = node_id

        # If we have a test node
        if (children_left[node_id] != children_right[node_id]):
            stack.append((children_left[node_id], parent_depth + 1))
            stack.append((children_right[node_id], parent_depth + 1))
        else:
            is_leaves[node_id] = True

    return [NodeRecord(*entry) for entry in zip(node_ids, node_depth, is_leaves, n_samples)]


def do_dataset_run_gon(ds: np.ndarray,
                       max_depth_experts: int,
                       n_experts: int,
                       max_depth_ass_tree: int,
                       min_samples_leaf_ass_tree: int,
                       n_runs: int,
                       experiment_id: str,
                       val_perc: Optional[float] = None,
                       ovr_mode: bool = False,
                       test_size: float = 0.33,
                       start_lasso: float = 0.1,
                       lasso_step: float = 0.05) -> List[RunEval]:
    records = []

    for i in range(n_runs):
        for feature_amount in [float(f'{str(f):.4}') for f in np.arange(start_lasso,
                                                                        1.05,
                                                                        lasso_step)]:
            start_time = time.monotonic()
            X_train, X_test, y_train, y_test = train_test_split(ds[:, :-1],
                                                                ds[:, -1],
                                                                test_size=test_size,
                                                                random_state=42 + i)

            pool = [DecisionTreeClassifier(max_depth=max_depth_experts,
                                           random_state=42 + i) for i in range(n_experts)]
            n_features = X_train.shape[1]
            lasso = round(n_features * feature_amount)
            print(f"Taking {lasso} features of {n_features} ({feature_amount * 100:.2f}%)")
            config_clf = {'pool_classifiers': pool,
                          'random_state': 42 + i,
                          'iterations': 20,
                          'max_jobs': 1,
                          'maximum_selected_features': lasso,
                          'val_perc': val_perc,
                          }
            explained_clf = GON(**config_clf)

            explained_clf.fit(X_train.copy(), y_train.copy())

            # (3) CREATE ASSIGNMENT PROBLEM
            assignment_problem = create_assignment_problem_gon(explained_clf,
                                                               X=X_train,
                                                               as_ovr=ovr_mode)

            trees_sc = generate_assignment_models(assignment_problem=assignment_problem,
                                                  X_for_expert=X_train[:,
                                                               explained_clf.get_important_feature_indexer()],
                                                  explanation_model=DecisionTreeClassifier,
                                                  explanation_model_params={
                                                      'max_depth': max_depth_ass_tree,
                                                      'min_samples_leaf': min_samples_leaf_ass_tree})

            # preds = calculate_assignments(assignment_models=trees_sc,
            #                              X_for_expert=X_test[:, explained_clf.get_important_feature_indexer()])

            # trues = create_assignment_problem_gon(dcs_classifier=explained_clf,
            #                                      X=X_test,
            #                                      as_ovr=ovr_mode)

            # .. accuracy
            test_acc, train_acc = explained_clf.score(X_test, y_test), explained_clf.score(X_train, y_train)

            # .. assignment accuracy
            acc_assignment_test = calculate_assignment_accuracy_gon(dcs_classifier=explained_clf,
                                                                    assignment_models=trees_sc,
                                                                    X_for_expert=X_test[:,
                                                                                 explained_clf.get_important_feature_indexer()],
                                                                    X=X_test)

            if acc_assignment_test <= 0.1:
                print("Doof")

            acc_assignment_train = calculate_assignment_accuracy_gon(dcs_classifier=explained_clf,
                                                                     assignment_models=trees_sc,
                                                                     X_for_expert=X_train[:,
                                                                                  explained_clf.get_important_feature_indexer()],
                                                                     X=X_train)

            # .. tree metrics
            nodes_assignment_trees = return_node_information(trees_sc[0])
            leafs = [*filter(lambda node: node.is_leaf, nodes_assignment_trees)]
            total_samples = sum([node.n_samples for node in leafs])
            avg_path_len = sum([node.depth * (node.n_samples / total_samples) for node in leafs])

            if avg_path_len < 1.:
                print("Oopsi2")

            end_time = time.monotonic()

            res = RunEval(ds_name=experiment_id,
                          feature_amount=feature_amount,
                          clf_name=GON.__class__.__name__,
                          params_expert=pool[0].get_params(),
                          params_model=config_clf,
                          params_ass_model=trees_sc[0].get_params(),
                          run_num=i,
                          run_time=end_time - start_time,
                          acc_test=test_acc,
                          acc_train=train_acc,
                          acc_train_assignment=acc_assignment_train,
                          acc_test_assignment=acc_assignment_test,
                          avg_path_len=avg_path_len,
                          ovr_assignment=ovr_mode,
                          date=datetime.datetime.now())

            records.append(res)
            print('train acc ass', res.acc_train_assignment, 'test acc ass', res.acc_test_assignment, '\n',
                  'train acc', res.acc_train, 'test acc', res.acc_test)

    return records


def prep_onthology_dataset(csv_path: str, for_label: Union[str, List[str]], encoding='latin-1') -> pd.DataFrame:
    """
    Will read the onthological CSV files and filter them to build a valid DataFrame
    
    @param csv_path: path to csv file
    @param for_label: column(s) to consider as label (will be appened as last column). If a list is given one column containing BOTH AND-conjunction of both will be used
    @paran encoding: Encoding of the CSV-file
    """
    
    # load data
    df = pd.read_csv(str(csv_path), encoding=encoding)
    
    if isinstance(for_label, str):
        for_label = [for_label]
    
    label_cols = df[for_label]
    
    # drop labels which are None
    try:
        false_labels = label_cols.isnull().any(axis=1)
        df = df[~false_labels]
        df[for_label] = df[for_label].astype(np.bool)
    except Exception as e:
            logging.getLogger().error(f"There was an error converting columns {label_cols}: {e} to bool (even after removing null-entries)")
            raise e
    
    # if multiple labels are given they have to be binary in order to combine them
    if len(for_label) > 1:
        for label in for_label:
            assert label in df.columns, f"Column {label} does not exist in csv header (Code: 832478293)"
            assert pd.api.types.is_bool_dtype(df[label]), f"Column {label} is not a boolean column (is: {df[label].dtype}). I don't know how I can combine them (Code: 382348023)"

    # filter not having G_datum
    df = df[df.G_Datum.notna()]

    # add age column
    df['age'] = df['G_Datum'].map(lambda d: d[:4]).astype(np.int) - df['Geburtsdatum'].values.astype(np.int)

    # filter n/a values in Visus (V_*)
    df = df[df.V_Vis_L.notna() & 
                          df.V_Sph_L.notna() & 
                          df.V_Zyl_L.notna() & 
                          df.V_Achse_L.notna() & 
                          df.V_Vis_R.notna() & 
                          df.V_Sph_R.notna() & 
                          df.V_Zyl_R.notna() & 
                          df.V_Achse_R.notna()]
    
    # extract label columns
    label_col = df[for_label[0]]
    if len(for_label) > 0:
        for label in for_label[1:]:
            label_col |= df[label]
    
    # Generate column name(!) 
    label_col_name = '_OR_'.join(for_label)
    
    # filter columns       
    df = df[['Geschlecht',
            'G_Apoplex',
            'G_ArterielleHypertonie',
            'G_DiabetisMellitus','G_Herzinfarkt','G_Blutverd',
            'V_Vis_L',
            'V_Sph_L',
            'V_Zyl_L',
            'V_Achse_L',
            'V_Vis_R',
            'V_Sph_R',
            'V_Zyl_R',
            'V_Achse_R',
            'age']]
    
    df[label_col_name] = label_col
    
    # correct data type from object to float and replace "-"-entries with -1000000
    columns_wrong = [*df.columns][1:-2]
    for col in columns_wrong:
        try:
            column_data = df[col].map(lambda val: val if val != '-' else -1000000)
            df[col] = column_data.astype(np.float)
        except ValueError as e:
            logging.getLogger().error(f"There was an error converting column {col}: {e}")
            raise e

    # replace NaN with false
    df = df.fillna(0)
    
    return df


class RipperAdapterRuleset():
    
    REGEX_TWO_NUMBERS_WITH_MINUS_BETWEEN = r'([+\-]?[0-9]+\.[0-9]+)-([+\-]?[0-9]+\.[0-9]+)'
    REGEX_TWO_NUMBERS_WITH_MINUS_BETWEEN_COMPILED = re.compile(REGEX_TWO_NUMBERS_WITH_MINUS_BETWEEN)
    
    def get_pandas_query_string(self, cond: wittgenstein.base.Cond) -> str:
        """
        Will regturn a string that can be used in pandas.query
        has the form lower_value <= `field_name` <= upper_value
        :return:
        """
        val = cond.val
        feature = cond.feature
        matches = self.REGEX_TWO_NUMBERS_WITH_MINUS_BETWEEN_COMPILED.match(val)
        if matches is None:
            raise Exception("Only rules with patterns num1-num2 are supported atm (Code: 2348723842)")

        try:
            num_1 = matches[1]
            num_2 = matches[2]

            num_1_float = float(num_1)
            num_2_float = float(num_2)

            return f"{num_1_float-0.005} < `{feature}` <= {num_2_float+0.0049}"
        
        except IndexError:
            raise Exception("Numbers could not be found by regex. Pattern: num1-num2 (Code: 324982390)")
            
        except ValueError:
            raise Exception(f"Could't parse the matches strings to numbers (Code: 0932482930)")
    
    def __init__(self, ripper: wittgenstein.RIPPER):
        self.rule_set = ripper.ruleset_
        self.ripper = ripper
        
    def count_conditions(self):
        i = 0
        for rule in self.rule_set:
            for clause in rule.conds:
                i+=1
                
        return i
    
    def count_rules(self):
        return len(self.rule_set)
    
    def calc_avg_rule_length(self):
        return self.count_conditions() / self.count_rules()
    
    
    def get_covered_samples(self, df: pd.DataFrame):
        
        """
        covered_ids = []
        for rule in self.rule_set.rules:
            
            # get samples matching all conditions
            # covered = df.query(self.get_pandas_query_string(cond=rule.conds[0]))
            queries = []
            for cond in rule.conds:
                queries.append(self.get_pandas_query_string(cond=cond))
            query_str = '('+') and ('.join(queries)+')'
            #print(query_str)
            working_indices = [*df.query(query_str).index]
            #print(working_indices)
            covered_ids += working_indices
            
        """
        cpy = df.copy()
        wittgenstein.base.bin_transform(cpy, self.ripper.bin_transformer_)
        covered_ids= self.rule_set.covers(cpy).index.tolist()
        return df.loc[[*set(covered_ids)], :]



class BRCGWrapper():
    """
    model.z -> Each column presents a combination of rules. The rules are the index(!) of z where the value is not 0;
            -> (n_clauses x n_rules)
    model.w -> index which rules are chosen in the system 
            -> (n_rules)
    """
    def __init__(self, brcg: BRCGExplainer):
        self.brcg_explainer = brcg
        self.model: BooleanRuleCG = self.brcg_explainer._model
        
    def get_chosen_rules(self) -> pd.DataFrame:
        return self.model.z.loc[:, self.model.w > 0.5]
        
    def count_rules(self) -> int:
        return self.get_chosen_rules().shape[1]
    
    def count_conditions(self) -> int:
        return self.get_chosen_rules().sum().sum()
    
    def calc_avg_rule_length(self) -> float:
        return self.count_conditions() / self.count_rules()
    
    def get_covered_samples(self, df_binarized: pd.DataFrame):
        # please never ask me what I did here ever again
        chosen_rules = self.get_chosen_rules()
        num_matching_conditions = df_binarized.dot(chosen_rules)
        difference_to_needed_rules = num_matching_conditions - chosen_rules.sum(axis=0)
        is_covered_by_rule = difference_to_needed_rules == 0
        is_covered_by_at_least_one_rule = is_covered_by_rule.sum(axis=1) > 0
        
        #chosen_rules = wrapper_brcg_pos_malignant.get_chosen_rules()
        #oks = (X_train_breast_binarized.dot(chosen_rules)-chosen_rules.sum(axis=0) == 0).sum(axis=1) > 0
        #X_train_breast_binarized.loc[oks[oks].index]
        
        return df_binarized.loc[is_covered_by_at_least_one_rule[is_covered_by_at_least_one_rule].index]
    
    
    
def get_leaf_depths_sklearn_tree(estimator: DecisionTreeClassifier) -> List[int]:
    n_nodes = estimator.tree_.node_count
    children_left = estimator.tree_.children_left
    children_right = estimator.tree_.children_right
    feature = estimator.tree_.feature
    threshold = estimator.tree_.threshold


    # The tree structure can be traversed to compute various properties such
    # as the depth of each node and whether or not it is a leaf.
    node_depth = np.zeros(shape=n_nodes, dtype=np.int64)
    is_leaves = np.zeros(shape=n_nodes, dtype=bool)
    stack = [(0, -1)]  # seed is the root node id and its parent depth
    
    leaf_depths = []
    while len(stack) > 0:
        node_id, parent_depth = stack.pop()
        node_depth[node_id] = parent_depth + 1

        # If we have a test node
        if (children_left[node_id] != children_right[node_id]):
            stack.append((children_left[node_id], parent_depth + 1))
            stack.append((children_right[node_id], parent_depth + 1))
        else:
            leaf_depths.append(parent_depth + 1)
    
    return leaf_depths